package com.company.departments;

import com.company.goods.BaseGoods;
import com.company.interfaces.IElectronicDevice;
import com.company.interfaces.IEmployee;
import com.company.interfaces.IGood;
import com.company.service.BaseEmployee;

import java.util.ArrayList;

public class ElectronicDepartment extends BaseDepartment {

    public ElectronicDepartment(String name, ArrayList<IGood> goodList, ArrayList<IEmployee> employeeList) {
        super(name, goodList, employeeList);
    }
}

