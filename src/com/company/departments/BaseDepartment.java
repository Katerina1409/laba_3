package com.company.departments;

import com.company.clients.BaseVisitor;
import com.company.goods.BaseGoods;
import com.company.interfaces.IDepartment;
import com.company.interfaces.IEmployee;
import com.company.interfaces.IGood;
import com.company.service.BaseEmployee;

import java.util.ArrayList;

public abstract class BaseDepartment implements IDepartment {
    private String name;
    ArrayList<IGood> goodList;
    ArrayList<IEmployee> employeeList;

    public BaseDepartment(String name, ArrayList<IGood> goodList, ArrayList<IEmployee> employeeList) {
        this.name = name;
        this.goodList = goodList;
        this.employeeList = employeeList;
    }

    public void addEmployee() {}

    public void addGood() {}

    public String getName() {
        return name;
    }

    public ArrayList<IGood> getGoodList() {
        return goodList;
    }

    public ArrayList<IEmployee> getEmployeeList() {
        return employeeList;
    }
}


