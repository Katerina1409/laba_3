package com.company.service;

import com.company.departments.BaseDepartment;

public class Consultant extends BaseEmployee{
    private boolean free;

    public Consultant(String name,boolean free,BaseDepartment department){
        super(name, free, department);
    }

    public void consult(){}

    public void send(){}

    public boolean isFree(){
        return free;
    }
}
