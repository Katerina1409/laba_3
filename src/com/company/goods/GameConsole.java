package com.company.goods;

import com.company.interfaces.IDepartment;

public class GameConsole extends ElectronicDevice {
    private String ram;

    public GameConsole(IDepartment department, String name, boolean hasGuarantee, String price, String company, String ram) {
        super(department, name, hasGuarantee, price, company);
        this.ram = ram;
    }


    public void on(){}

    public void loadGame(){}
}
